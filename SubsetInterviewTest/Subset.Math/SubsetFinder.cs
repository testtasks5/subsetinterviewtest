﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubsetMath
{
    public class SubSetFinder
    {

        /// <summary>
        /// Returns list of elements for the <paramref name="inputSet"/> whose sum 
        /// is equal to  <paramref name="value"/> or most close to it.
        /// </summary>
        /// <remarks>
        /// If the desired <paramref name="value"/> is not found, than either the nearest sum or an empty list will be returned.
        /// </remarks>
        /// <typeparam name="T">The type of the elements in the input <paramref name="inputSet"/></typeparam>
        /// <param name="inputSet">The initial collection in which the search will occur.</param>
        /// <param name="value"> desired sum.</param>
        /// <param name="step">The step by which the values will change while searching for a suitable amount</param>
        /// <returns>Returns found collection of elements form <paramref name="inputSet"/>.</returns>
        public List<T> GetSubset<T>(List<T> inputSet, decimal value, decimal step) where T : IQuantity
        {
           throw new NotImplementedException();
        }
    }
}
